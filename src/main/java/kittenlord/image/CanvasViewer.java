package kittenlord.image;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

public class CanvasViewer implements Runnable {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new CanvasViewer());
	}

	@Override
	public void run() {
		var tilesX = 8;
		var tilesY = 8;
		var tiles = Stream.of("tile_white.png", "tile_black.png")
				.map(ImageResources::loadResourceImage)
				.map(Optional::get)
				.collect(Collectors.toList());
		var pieces = Stream.of("piece_white.png", "piece_black.png")
				.map(ImageResources::loadResourceImage)
				.map(Optional::get)
				.collect(Collectors.toList());
		var w = tiles.get(0).getWidth();
		var h = tiles.get(0).getHeight();

		@SuppressWarnings("serial")
		var canvas = new Canvas() {

			@Override
			public void paint(Graphics g) {
				for (int y = 0; y < tilesY; y++) {
					for (int x = 0; x < tilesX; x++) {
						g.drawImage(tiles.get((x + y) % tiles.size()), x * w, y * h, null);
						if ((0 == y) || (1 == y)) {
							g.drawImage(pieces.get(1), x * w, y * h, null);
						}
						if ((6 == y) || (7 == y)) {
							g.drawImage(pieces.get(0), x * w, y * h, null);
						}
					}
				}
			}

		};
		canvas.setPreferredSize(new Dimension(tilesX * w, tilesY * h));

		var frame = new JFrame("Canvas Viewer");
		ImageResources.loadResourceImage("app.png").ifPresent(frame::setIconImage);
		frame.add(canvas);
		frame.pack();
		frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		frame.setVisible(true);
	}

}
