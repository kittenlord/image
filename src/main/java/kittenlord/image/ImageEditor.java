package kittenlord.image;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

public class ImageEditor implements Runnable {

	private JFrame appFrame;
	private JLabel imageComponent;
	private JFileChooser fileChooser = new JFileChooser();
	private Optional<BufferedImage> currentImage = Optional.empty();

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new ImageEditor());
	}

	@Override
	public void run() {
		imageComponent = new JLabel();
		appFrame = new JFrame("Image Editor");
		ImageResources.loadResourceImage("app.png").ifPresent(appFrame::setIconImage);
		appFrame.setJMenuBar(createMenu());
		appFrame.add(imageComponent);
		appFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		appFrame.setVisible(true);
		showImage(ImageResources.loadExampleResourceImage());
	}

	private JMenuBar createMenu() {
		var fileMenu = new JMenu("File");
		fileMenu.add(menuItem("Open...", this::openFileDialog));
		fileMenu.add(menuItem("Save as...", this::saveFileDialog));
		fileMenu.addSeparator();
		fileMenu.add(menuItem("Exit", () -> appFrame.dispose()));

		var imageMenu = new JMenu("Image");
		imageMenu.add(menuItemCreate("Transparent", () -> ImageEffects.createTransparentImage(100, 100)));
		imageMenu.add(menuItemCreate("Red-green gradient", () -> ImageEffects.createRedGreenGradient()));
		imageMenu.add(menuItemCreate("Example resource", () -> ImageResources.loadExampleResourceImage().get()));

		var effectMenu = new JMenu("Effect");
		effectMenu.add(menuItemEffect("Swap red <-> green", img -> ImageEffects.swapRedGreen(currentImage.get())));
		effectMenu.add(menuItemEffect("Paint random red dots", img -> ImageEffects.paintRandomRedDots(img, 1000)));
		effectMenu
				.add(menuItemEffect("Paint random blue circles", img -> ImageEffects.paintRandomBlueCircles(img, 100)));
		effectMenu.add(menuItemEffect("Rotate clockwise", img -> ImageEffects.rotateClockwise(img)));

		var helpMenu = new JMenu("Help");
		helpMenu.add(menuItem("About", () -> JOptionPane.showMessageDialog(appFrame,
				"Image Editor\n\n\u00a9 2021 Kittenlord", "About Image Editor", JOptionPane.INFORMATION_MESSAGE)));

		var bar = new JMenuBar();
		bar.add(fileMenu);
		bar.add(imageMenu);
		bar.add(effectMenu);
		bar.add(helpMenu);
		return bar;
	}

	private JMenuItem menuItem(String text, Runnable action) {
		var item = new JMenuItem(text);
		item.addActionListener(event -> action.run());
		return item;
	}

	private JMenuItem menuItemCreate(String text, Supplier<BufferedImage> create) {
		return menuItem(text, () -> showImage(create.get()));
	}

	private JMenuItem menuItemEffect(String text, Function<BufferedImage, BufferedImage> effect) {
		return menuItem(text, () -> currentImage.ifPresent(img -> showImage(effect.apply(img))));
	}

	/**
	 * Displays given image, or an error message if the image is not available.
	 */
	private void showImage(Optional<BufferedImage> image) {
		image.ifPresentOrElse(this::showImage, () -> this.showMessage("Error loading image"));
	}

	private void showImage(BufferedImage image) {
		currentImage = Optional.of(image);
		imageComponent.setIcon(new ImageIcon(image));
		imageComponent.setText(null);
		appFrame.pack();
	}

	private void showMessage(String message) {
		currentImage = Optional.empty();
		imageComponent.setIcon(null);
		imageComponent.setText(message);
		appFrame.pack();
	}

	private void openFileDialog() {
		fileChooser.setDialogTitle("Open image...");
		if (JFileChooser.APPROVE_OPTION == fileChooser.showOpenDialog(appFrame)) {
			showImage(ImageResources.loadImage(fileChooser.getSelectedFile()));
		}
	}

	private void saveFileDialog() {
		if (!currentImage.isPresent()) {
			return;
		}
		fileChooser.setDialogTitle("Save image as...");
		if (JFileChooser.APPROVE_OPTION == fileChooser.showSaveDialog(appFrame)) {
			var file = fileChooser.getSelectedFile();
			if (file.exists()) {
				if (JOptionPane.YES_OPTION != JOptionPane.showConfirmDialog(appFrame,
						"File exists. Do you want to overwrite it?")) {
					return;
				}
			}
			try {
				ImageResources.saveImage(file, currentImage.get());
				JOptionPane.showMessageDialog(appFrame, "File saved", "File saved",
						JOptionPane.INFORMATION_MESSAGE);
			} catch (IOException e) {
				JOptionPane.showMessageDialog(appFrame, e.getMessage(), "Error saving file", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

}
