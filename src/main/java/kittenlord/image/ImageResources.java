package kittenlord.image;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Optional;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 * Static methods for loading and saving images.
 */
public class ImageResources {

	private static final String EXAMPLE_RESOURCE_FILE_NAME = "daisies.jpg";

	/**
	 * Returns an image loaded from given file in application resources, if available.
	 */
	public static Optional<BufferedImage> loadResourceImage(String imageFileName) {
		try (var is = ImageEffects.class.getResourceAsStream(imageFileName)) {
			if (null == is) {
				return Optional.empty();
			}
			return Optional.of(ImageIO.read(is));
		} catch (IOException e) {
			return Optional.empty();
		}
	}

	/**
	 * Returns an icon loaded from given file in application resources, if available.
	 */
	public static Optional<Icon> loadResourceIcon(String imageFileName) {
		return loadResourceImage(imageFileName).map(ImageIcon::new);
	}

	/**
	 * Returns an example image loaded from application resources, if available.
	 * 
	 * @see https://unsplash.com/photos/CVoa6nTXuL4
	 */
	public static Optional<BufferedImage> loadExampleResourceImage() {
		return loadResourceImage(EXAMPLE_RESOURCE_FILE_NAME);
	}

	/**
	 * Returns an example icon loaded from application resources, if available.
	 */
	public static Optional<Icon> loadExampleResourceIcon() {
		return loadExampleResourceImage().map(ImageIcon::new);
	}

	/**
	 * Returns an image loaded from given external file, if available.
	 */
	public static Optional<BufferedImage> loadImage(File imageFile) {
		try {
			return Optional.ofNullable(ImageIO.read(imageFile));
		} catch (IOException e) {
			return Optional.empty();
		}
	}

	/**
	 * Saves an image to given external file. Throws an exception if saving fails.
	 */
	public static void saveImage(File imageFile, BufferedImage image) throws IOException {
		String format = fileExtension(imageFile.getName()).orElse("png");
		var ok = ImageIO.write(image, format, imageFile);
		if (!ok) {
			throw new IOException("Unsupported image format: " + format);
		}
	}

	static Optional<String> fileExtension(String fileName) {
		int p = fileName.lastIndexOf('.');
		return (p < 0) ? Optional.empty() : Optional.of(fileName.substring(p + 1));
	}

}
