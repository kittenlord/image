package kittenlord.image;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.Random;

public class ImageEffects {

	private static Random random = new Random();

	/**
	 * Returns the pixel color in TYPE_INT_ARGB format with given components.
	 */
	static int argb(final int alpha, final int r, final int g, final int b) {
		return ((alpha << 24) & 0xff000000) | ((r << 16) & 0xff0000) | ((g << 8) & 0xff00) | (b & 0xff);
	}

	/**
	 * Returns the alpha component from a pixel color, in range from 0 (transparent) to 255.
	 */
	static int a(final int argb) {
		return (argb >> 24) & 0xff;
	}

	/**
	 * Returns the red component from a pixel color, in range from 0 to 255.
	 */
	static int r(final int argb) {
		return (argb >> 16) & 0xff;
	}

	/**
	 * Returns the green component from a pixel color, in range from 0 to 255.
	 */
	static int g(final int argb) {
		return (argb >> 8) & 0xff;
	}

	/**
	 * Returns the blue component from a pixel color, in range from 0 to 255.
	 */
	static int b(final int argb) {
		return argb & 0xff;
	}

	public static BufferedImage createTransparentImage(int width, int height) {
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				image.setRGB(x, y, argb(0, 0, 0, 0));
			}
		}
		return image;
	}

	/**
	 * Creates an image with color gradient with red component increasing along its X axis, and green component
	 * increasing along its Y axis.
	 */
	public static BufferedImage createRedGreenGradient() {
		BufferedImage image = new BufferedImage(256, 256, BufferedImage.TYPE_INT_ARGB);
		for (int y = 0; y < 256; y++) {
			for (int x = 0; x < 256; x++) {
				image.setRGB(x, y, argb(255, x, y, 0));
			}
		}
		return image;
	}

	/**
	 * Creates a new image based on given image with red and green components swapped.
	 */
	public static BufferedImage swapRedGreen(final BufferedImage originalImage) {
		int w = originalImage.getWidth();
		int h = originalImage.getHeight();
		BufferedImage image = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		for (int y = 0; y < h; y++) {
			for (int x = 0; x < w; x++) {
				final int color = originalImage.getRGB(x, y);
				image.setRGB(x, y, argb(a(color), g(color), r(color), b(color)));
			}
		}
		return image;
	}

	public static BufferedImage paintRandomRedDots(BufferedImage image, int numberOfDots) {
		final int w = image.getWidth();
		final int h = image.getHeight();
		for (int i = 0; i < numberOfDots; i++) {
			image.setRGB(random.nextInt(w), random.nextInt(h), argb(255, 255, 0, 0));
		}
		return image;
	}

	public static BufferedImage paintRandomBlueCircles(BufferedImage image, int numberOfCircles) {
		Graphics g = image.getGraphics();
		g.setColor(Color.BLUE);
		final int w = image.getWidth();
		final int h = image.getHeight();
		for (int i = 0; i < numberOfCircles; i++) {
			int r = 5 + random.nextInt(10);
			g.drawOval(random.nextInt(w), random.nextInt(h), r, r);
		}
		return image;
	}

	public static BufferedImage rotateClockwise(BufferedImage originalImage) {
		int w = originalImage.getWidth();
		int h = originalImage.getHeight();
		BufferedImage image = new BufferedImage(h, w, BufferedImage.TYPE_INT_ARGB);
		for (int y = 0; y < h; y++) {
			for (int x = 0; x < w; x++) {
				image.setRGB(h - y - 1, x, originalImage.getRGB(x, y));
			}
		}
		return image;
	}

}
