package kittenlord.image;

import java.awt.Component;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

public class ImageViewer implements Runnable {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new ImageViewer());
	}

	@Override
	public void run() {
		var frame = new JFrame("Image Viewer");
		ImageResources.loadResourceImage("app.png").ifPresent(frame::setIconImage);
		frame.setJMenuBar(createMenuBar(frame));
		frame.add(createImageComponent());
		frame.pack();
		frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		frame.setVisible(true);
	}

	private Component createImageComponent() {
		return ImageResources.loadExampleResourceIcon()
				.map(JLabel::new)
				.orElseGet(() -> new JLabel("Image not found"));
	}

	private JMenuBar createMenuBar(JFrame frame) {
		var about = new JMenuItem("About");
		ImageResources.loadResourceIcon("icon_about.png").ifPresent(about::setIcon);
		about.addActionListener(e -> JOptionPane.showMessageDialog(frame, "Image Viewer\n\n\u00a9 2021 Kittenlord", "About Image Viewer", JOptionPane.INFORMATION_MESSAGE));
		var exit = new JMenuItem("Exit");
		ImageResources.loadResourceIcon("icon_exit.png").ifPresent(exit::setIcon);
		exit.addActionListener(e -> frame.dispose());
		var app = new JMenu("Application");
		app.add(about);
		app.add(exit);
		var menuBar = new JMenuBar();
		menuBar.add(app);
		return menuBar;
	}

}
