package kittenlord.image;

import static org.assertj.core.api.Assertions.assertThat;

import java.awt.image.BufferedImage;

import org.junit.jupiter.api.Test;

class ImageEffectsTest {

	static final int BLACK = 0xff_000000;
	static final int RED = 0xff_ff0000;
	static final int GREEN = 0xff_00ff00;
	static final int YELLOW = 0xff_ffff00;

	@Test
	void testCreateRedGreenGradient() {
		var image = ImageEffects.createRedGreenGradient();
		assertThat(image.getRGB(0, 0)).isEqualTo(BLACK);
		assertThat(image.getRGB(255, 0)).isEqualTo(RED);
		assertThat(image.getRGB(0, 255)).isEqualTo(GREEN);
		assertThat(image.getRGB(255, 255)).isEqualTo(YELLOW);
	}

	@Test
	void testSwapRedGreen() {
		BufferedImage image = ImageEffects.swapRedGreen(ImageEffects.createRedGreenGradient());
		assertThat(image.getRGB(0, 0)).isEqualTo(BLACK);
		assertThat(image.getRGB(255, 0)).isEqualTo(GREEN);
		assertThat(image.getRGB(0, 255)).isEqualTo(RED);
		assertThat(image.getRGB(255, 255)).isEqualTo(YELLOW);
	}
	
	@Test
	void testRotateClockwise() {
		BufferedImage image = ImageEffects.rotateClockwise(ImageEffects.createRedGreenGradient());
		assertThat(image.getRGB(0, 0)).isEqualTo(GREEN);
		assertThat(image.getRGB(255, 0)).isEqualTo(BLACK);
		assertThat(image.getRGB(0, 255)).isEqualTo(YELLOW);
		assertThat(image.getRGB(255, 255)).isEqualTo(RED);
	}

}
