package kittenlord.image;

import static org.assertj.core.api.Assertions.*;
import org.junit.jupiter.api.Test;

class ImageResourcesTest {

	@Test
	void testLoadResourceImage() {
		assertThat(ImageResources.loadExampleResourceImage()).isPresent();
		assertThat(ImageResources.loadResourceImage("app.png")).isPresent();
		assertThat(ImageResources.loadResourceImage("nothing.png")).isEmpty();
	}

	@Test
	void testLoadResourceIcon() {
		assertThat(ImageResources.loadExampleResourceIcon()).isPresent();
		assertThat(ImageResources.loadResourceIcon("app.png")).isPresent();
		assertThat(ImageResources.loadResourceIcon("nothing.png")).isEmpty();
	}

	@Test
	void testFileExtension() {
		assertThat(ImageResources.fileExtension("something")).isEmpty();
		assertThat(ImageResources.fileExtension("something.png")).contains("png");
		assertThat(ImageResources.fileExtension("something.jpg")).contains("jpg");
		assertThat(ImageResources.fileExtension("something.bmp")).contains("bmp");
		assertThat(ImageResources.fileExtension("something.gif")).contains("gif");
	}

}
